import React from 'react'

const CircularProg = props => (
	<div
		style={{
			position: 'relative',
			textAlign: 'center',
			width: '100%',
			zIndex: 1000
		}}
	>
		<div className="loader" />
	</div>
)

export default CircularProg