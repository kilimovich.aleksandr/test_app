import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

import { searchTracks } from '../reducers/trackReducer'
import CircularProg from './circularProg'
import CustomSnackbar from './snackbar'

class SearchTrack extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.searchedTracks !== nextProps.searchedTracks ||
      this.props.inProgress !== nextProps.inProgress ||
      this.props.snackState !== nextProps.snackState
    )
  }

  render() {
    const {
      searchTracks,
      searchedTracks,
      inProgress,
      snackState,
    } = this.props
    return (
      <div className="block-wrapper">
        <div className="searchTrack">
          <div>Поиск трека по названию</div>
          <div className="inputWrapper">
            <input
              type="text"
              className="customInput"
              placeholder="Введите название трека (минимум 3 символа):"
              onChange={event => {
                event.target.value.length > 2 &&
                  searchTracks({ name: event.target.value })
              }}
            />
            <div className="inputLine" />
          </div>
          {inProgress ? (
            <CircularProg />
          ) : (
            searchedTracks &&
            searchedTracks.length > 0 && (
              <div>
                {_.map(searchedTracks, (item, key) => {
                  return (
                    <div key={key}>
                      {item.name && item.name}: {item.artist && item.artist}
                    </div>
                  )
                })}
              </div>
            )
          )}
        </div>
        <CustomSnackbar  
          open={snackState.open}
          message={snackState.message}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  inProgress: state.trackReducer.inProgress,
  searchedTracks: state.trackReducer.searchedTracks,
  snackState: state.snackReducer
})

const mapDispatchToProps = {
  searchTracks: searchTracks.request
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchTrack)