import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pagination from 'react-js-pagination'

import TopTracksTable from './topTracksTable'
import { getTopTracks, changeAtrObj } from '../reducers/trackReducer'
import CircularProg from './circularProg'
import Button from './button'
import CustomSnackbar from './snackbar'

class Home extends Component {
  componentWillMount() {
    this.props.getTopTracks({ page: 1 })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.topTracks !== nextProps.topTracks ||
      this.props.inProgress !== nextProps.inProgress ||
      this.props.snackState !== nextProps.snackState
    )
  }

  handlePageChange = pageNumber => {
    this.props.changeAtrObj({ field: 'page', value: pageNumber })
    this.props.getTopTracks({ page: pageNumber })
  }

  render() {
    const { topTracks, snackState, inProgress } = this.props
    return (
      <div className="home">
        {inProgress ? (
          <CircularProg />
        ) : (
          <div className="block-wrapper" style={{ maxWidth: '550px' }}>
            {topTracks.tracks && topTracks.tracks.length > 0 ? (
              <TopTracksTable
                tracks={topTracks.tracks}
                history={this.props.history}
              />
            ) : (
              <p>Извините, на этой странице треков не найдено</p>
            )}
            <Pagination
              activePage={+topTracks.attr.page}
              itemsCountPerPage={+topTracks.attr.perPage}
              totalItemsCount={+topTracks.attr.total}
              pageRangeDisplayed={10}
              onChange={this.handlePageChange}
              activeClass="activePage"
              activeLinkClass="activeLinkClass"
            />
            <Button link="/search_track" label="Поиск трека" />
          </div>
        )}
        <CustomSnackbar open={snackState.open} message={snackState.message} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  topTracks: state.trackReducer.topTracks,
  inProgress: state.trackReducer.inProgress,
  snackState: state.snackReducer
})

const mapDispatchToProps = {
  getTopTracks: getTopTracks.request,
  changeAtrObj: changeAtrObj
}
export default connect(mapStateToProps, mapDispatchToProps)(Home)