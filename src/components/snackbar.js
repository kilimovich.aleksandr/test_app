import React, { Component } from 'react'

class customSnackbar extends Component {
	shouldComponentUpdate(nextProps) {
		return (
			this.props.open !== nextProps.open ||
			this.props.message !== nextProps.message
		)
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.open) {
			this.openSnackbar()
		}
	}
	
	openSnackbar() {
		var x = document.getElementById('snackbar')
		if (x) {
			x.className = 'show'
			setTimeout(function() {
				x.className = x.className.replace('show', '')
			}, 3000)
		}
	}

	render() {
		return (
			<div id="snackbar" className="customSnackBar">
				{this.props.message}
			</div>
		)
	}
}

export default customSnackbar