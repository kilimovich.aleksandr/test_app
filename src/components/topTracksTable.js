import React from 'react'
import _ from 'lodash'

const TopTracksTable = props => (
		<ul className='topTracksList'>
			{_.map(props.tracks, (item, key) => (
				<li className='track' key={key}>
					<div className='name'>{item.name ? item.name : '-'}</div>
					<div className='artist'
						onTouchTap={e => {
							props.history.push(
								`/artists/${item.artist.name}`,
								item.artist
							)
						}}
					>
						{item.artist.name ? item.artist.name : '-'}
					</div>
					<div className='image'>
						{item.image ? <img src={item.image} alt="img" /> : '-'}
					</div>
					<div className="artist-url">
						{item.artist && item.artist.url ? (
							<a href={item.artist.url}>{item.artist.url}</a>
						) : (
							'-'
						)}
					</div>
				</li>
			))}
		</ul>
)

export default TopTracksTable