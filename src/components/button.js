import React from 'react'
import { Link } from 'react-router-dom'

const Button = props => (
	<div className="homeButton">
          <Link to={props.link}>
          	<button className="customButton">{props.label}</button>
          </Link>
        </div>
)

export default Button