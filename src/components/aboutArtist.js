import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

import { getArtistInfo } from '../reducers/trackReducer'
import CircularProg from './circularProg'
import Button from './button'

class AboutArtist extends Component {
  componentWillMount() {
    this.props.getArtistInfo({
      name: this.props.match.params.name
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.artistInfo !== nextProps.artistInfo ||
      this.props.inProgress !== nextProps.inProgress
    )
  }

  render() {
    const { artistInfo, inProgress } = this.props
    return (
      <div className="block-wrapper">
        {inProgress ? (
          <CircularProg />
        ) : (
          artistInfo && (
            <div className="artist">
              <div className="name">{artistInfo.name && artistInfo.name}</div>
              <div className="image">
                {artistInfo.image && (
                  <img src={artistInfo.image} alt={artistInfo.name} />
                )}
              </div>
              <div className="tags">
                {artistInfo.tags &&
                  artistInfo.tags.length > 0 &&
                  _.map(artistInfo.tags, item => {
                    return `#${item.name} `
                  })}
              </div>

              {artistInfo.summary && (
                <div
                  className="description"
                  dangerouslySetInnerHTML={{
                    __html: artistInfo.summary
                  }}
                />
              )}
            </div>
          )
        )}
        <Button link="/" label="На главную" />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  inProgress: state.trackReducer.inProgress,
  artistInfo: state.trackReducer.artistInfo
})

const mapDispatchToProps = {
  getArtistInfo: getArtistInfo.request
}
export default connect(mapStateToProps, mapDispatchToProps)(AboutArtist)