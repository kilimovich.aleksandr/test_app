import { put, call } from 'redux-saga/effects'
import _ from 'lodash'
import {
	getTopTracks,
	getArtistInfo,
	searchTracks
} from '../reducers/trackReducer'
import {
	getTopTracksApi,
	getArtistInfoApi,
	searchTrackApi
} from '../api/tracks.js'
import { openSnackBar } from '../reducers/snackReducer.js'

export function* getTopTracksWorker({ payload }) {
	try {
		const { data } = yield call(getTopTracksApi, { page: payload.page })
		if (
			data &&
			data.tracks &&
			data.tracks['@attr'] &&
			data.tracks.track.length > 0
		) {
			let clearData = _.map(data.tracks.track, item => {
				return {
					artist: {
						name: item.artist.name ? item.artist.name : null,
						url: item.artist.url ? item.artist.url : null
					},
					image: item.image[0]['#text']
						? item.image[0]['#text']
						: null,
					name: item.name ? item.name : null
				}
			})
			yield put(
				getTopTracks.success({
					attr: data.tracks['@attr'],
					tracks: clearData
				})
			)
		} else {
			yield put(getTopTracks.failure())
			yield put(
				openSnackBar({
					message: 'Что-то пошло не так, попробуйте еще раз'
				})
			)
		}
	} catch (err) {
		yield put(getTopTracks.failure(err))
		yield put(
			openSnackBar({
				message: 'Что-то пошло не так, попробуйте еще раз'
			})
		)
	}
}

export function* getArtistInfoWorker({ payload }) {
	try {
		const { data } = yield call(getArtistInfoApi, {
			name: payload.name
		})
		if (data && data.artist) {
			let clearData = {
				name: data.artist.name ? data.artist.name : null,
				image:
					data.artist.image[3] && data.artist.image[3]['#text']
						? data.artist.image[3]['#text']
						: null,
				tags:
					data.artist.tags.tag && data.artist.tags.tag
						? data.artist.tags.tag
						: null,
				summary:
					data.artist.bio && data.artist.bio.summary
						? data.artist.bio.summary
						: null
			}
			yield put(getArtistInfo.success(clearData))
		} else {
			yield put(getArtistInfo.failure())
			yield put(
				openSnackBar({
					message: 'Что-то пошло не так, попробуйте еще раз'
				})
			)
		}
	} catch (err) {
		yield put(getArtistInfo.failure(err))
		yield put(
			openSnackBar({
				message: 'Что-то пошло не так, попробуйте еще раз'
			})
		)
	}
}

export function* searchTrackWorker({ payload }) {
	try {
		const { data } = yield call(searchTrackApi, { track: payload.name })
		if (
			data &&
			data.results &&
			data.trackmatches &&
			data.results.trackmatches.track &&
			data.results.trackmatches.track.length === 0
		) {
			yield put(
				openSnackBar({
					message: 'По вашему запросу ничего не найдено'
				})
			)
			yield put(searchTracks.success([]))
		} else {
			let clearData = _.map(data.results.trackmatches.track, item => {
				return {
					name: item.name ? item.name : null,
					artist: item.artist ? item.artist : null
				}
			})
			yield put(searchTracks.success(clearData))
		}
	} catch (err) {
		yield put(searchTracks.failure(err))
		yield put(
			openSnackBar({
				message: 'Что-то пошло не так, попробуйте еще раз'
			})
		)
	}
}