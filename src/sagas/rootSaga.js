import { takeLatest, all } from 'redux-saga/effects'
import {
	getTopTracks,
	getArtistInfo,
	searchTracks
} from '../reducers/trackReducer'
import {
	getTopTracksWorker,
	getArtistInfoWorker,
	searchTrackWorker
} from './trackSaga.js'

export default function* rootSaga() {
	yield all([
		takeLatest(getTopTracks.request, getTopTracksWorker),
		takeLatest(getArtistInfo.request, getArtistInfoWorker),
		takeLatest(searchTracks.request, searchTrackWorker)
	])
}