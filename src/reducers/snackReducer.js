import { createReducer, createAction } from 'redux-act'

const initialState = {
	open: false,
	message: ''
}

export const openSnackBar = createAction('SNACKBAR_OPENED')
export const closeSnackBar = createAction('SNACKBAR_CLOSED')

export default createReducer(
	{
		[openSnackBar]: (state, payload) => ({ ...state, open: true, message: payload.message }),
		[closeSnackBar]: (state, payload) => ({ ...state, open: false, message: '' })
	},
	initialState
)
