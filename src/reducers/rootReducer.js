import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import trackReducer from './trackReducer'
import snackReducer from './snackReducer'

export const rootReducer = combineReducers({
	trackReducer,
	snackReducer,
	routing: routerReducer,

})
