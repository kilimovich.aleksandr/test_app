import { createReducer } from 'redux-act'
import { createActions } from 'redux-act-extra'
import { createAction } from 'redux-act'

const initialState = {
  topTracks: {
    attr: {
      page: 1,
      perPage: 50,
      total: 1,
      totalPages: 1
    },
    tracks: []
  },
  artistInfo: null,
  searchedTracks: []
}

export const getTopTracks = createActions('GET_TOP_TRACKS')
export const getArtistInfo = createActions('GET_ARTIST_INFO')
export const searchTracks = createActions('SEARCH_TRACKS')

export const changeAtrObj = createAction('FIELD_WAS_CHANGED')

export default createReducer(
  {
    [changeAtrObj]: (state, payload) => ({
      ...state,
      topTracks: {
        ...state.topTracks,
        attr: {
          ...state.topTracks.attr,
          [payload.field]: payload.value
        }
      }
    }),

    [getTopTracks.request]: state => ({
      ...state,
      inProgress: true
    }),
    [getTopTracks.failure]: (state, payload) => ({
      ...state,
      inProgress: false
    }),
    [getTopTracks.success]: (state, payload) => ({
      ...state,
      inProgress: false,
      topTracks: {
        ...state.topTracks,
        attr: payload.attr,
        tracks: payload.tracks
      }
    }),

    [getArtistInfo.request]: state => ({
      ...state,
      inProgress: true,
      artistInfo: null
    }),
    [getArtistInfo.failure]: (state, payload) => ({
      ...state,
      inProgress: false
    }),
    [getArtistInfo.success]: (state, payload) => ({
      ...state,
      inProgress: false,
      artistInfo: payload
    }),

    [searchTracks.request]: state => ({
      ...state,
      inProgress: true
    }),
    [searchTracks.failure]: (state, payload) => ({
      ...state,
      inProgress: false
    }),
    [searchTracks.success]: (state, payload) => ({
      ...state,
      inProgress: false,
      searchedTracks: payload
    })
  },
  initialState
)