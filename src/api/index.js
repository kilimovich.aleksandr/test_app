import axios from 'axios'
import { API_URL, API_KEY } from '../config'

const mainApi = axios.create({
	baseURL: API_URL,
	params: {
		api_key: API_KEY,
		format: 'json'
	}
})

export default mainApi