import api from './index'

export const getTopTracksApi = params =>
	api.get(`2.0/?method=chart.gettoptracks`, {
		params: {
			page: params.page,
			limit: 50
		}
	})

export const getArtistInfoApi = params =>
	api.get(`2.0/?method=artist.getinfo`, {
		params: {
			artist: params.name
		}
	})

export const searchTrackApi = params =>
	api.get(`2.0/?method=track.search`, {
		params: {
			limit: 10,
			track: params.track
		}
	})