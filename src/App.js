import React from 'react'
import { Provider } from 'react-redux'
import { routerMiddleware } from 'react-router-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'
import createBrowserHistory from 'history/createBrowserHistory'
import { createStore, applyMiddleware, compose } from 'redux'
import { Switch, Router, Route } from 'react-router-dom'
import createSagaMiddleware from 'redux-saga'
import rootSaga from './sagas/rootSaga'
import { rootReducer } from './reducers/rootReducer'
import Home from './components/home.js'
import AboutArtist from './components/aboutArtist.js'
import SearchTrack from './components/searchTrack.js'


const composeWithDevTools =
  process.env.NODE_ENV === 'development' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
const composeEnhancers = composeWithDevTools || compose

const sagaMW = createSagaMiddleware()
const browserHistory = createBrowserHistory()
const routerMW = routerMiddleware(browserHistory)
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(...[routerMW, sagaMW]))
)
sagaMW.run(rootSaga)
injectTapEventPlugin()

export const App = location => (
  <Provider store={store}>
      <Router history={browserHistory}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/artists/:name" component={AboutArtist} />
          <Route path="/search_track" component={SearchTrack} />
          <Route component={NoMatch} />
        </Switch>
      </Router>
  </Provider>
)

const NoMatch = ({ location }) => (
  <div>
    <h3>
      Ничего не найдено по запросу <code>{location.pathname}</code>
    </h3>
  </div>
)
